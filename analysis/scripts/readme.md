# Scripts

## Extracting ANZSCO numbers from PDFs

The following bash command, using poppler-utils will extract a list of ANZSCO
numbers contained within a PDF, to a `anzsco.txt` file:

```
pdf_file_path = "~/Documents/skillshortagelist.pdf"
pdftotext "$pdf_file_path" /tmp/buffer.txt && grep -o "[0-9]\{6\}" /tmp/buffer.txt > anzsco.txt
```

## Using extracted ANZSCO numbers

The `fill_table.py` script, when given the output of the extracted ANZSCO
numbers, will format them into a spreadsheet style, using the
`anzsco_table.csv` file as a template.

This data can then easily be copy pasted into a compilation of a list series,
such as the `immediate_skill_shortage_list_analysis.csv` file.

## Compiling multiple lists

To follow the skill shortages across multiple lists, and multiple generations
of lists, use the `compile_tables.py` script to extract multiple
`*_skill_shortage_list_analysis.csv` files into a single file, allowing for a
more hollistic analysis.
