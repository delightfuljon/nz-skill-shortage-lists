# Python 3.6.8

import sys
import pandas as pd

anzsco_table_paths = sys.argv[1:]

# Get tables
anzsco_tables = []
for path in anzsco_table_paths:
    anzsco_tables.append(pd.read_csv(path))

# Rearrange tables
rearranged_tables = []
for table in anzsco_tables:
    transposed_table = table.T

    renamed_table = pd.DataFrame()
    for index in transposed_table:
        column = transposed_table[index]
        name = column["ANZSCO number"]

        dates_only = column.iloc[2:]

        values = dates_only.values

        indexes = dates_only.index
        short_dates = []
        for date in indexes:
            short_date = "/".join(date.split("/")[1:])
            short_dates.append(short_date)

        rearranged_column = pd.Series(data=values, index=short_dates)

        renamed_table[name] = rearranged_column
    rearranged_tables.append(renamed_table)

# Convert index to date time, and resample missing values
resampled_tables = []
for table in rearranged_tables:
    table.index = pd.to_datetime(table.index, format="%m/%y")
    table = table.asfreq("M", method="pad")
    table.index = table.index.to_period("M")
    resampled_tables.append(table)

# Join tables
joined_table = pd.concat(resampled_tables)

# Eliminate duplicate indexes. If a column has a `true` value, record that
# column as true.
merging_table = pd.DataFrame(columns=joined_table.columns)
for index, row in joined_table.iterrows():

    if index in merging_table.index:

        new_row = row
        old_row = merging_table.loc[index]

        merging_index=[]
        merging_data=[]
        
        for item_index in new_row.index:
            if new_row[item_index] == True or old_row[item_index] == True:
                merging_index.append(item_index)
                merging_data.append(True)

            else:
                merging_index.append(item_index)
                merging_data.append(old_row[item_index])


        merging_row = pd.Series(merging_data, merging_index, name=index)
        merging_table.append(merging_row)
        
    else:
        merging_table = merging_table.append(row)

sorted_table = merging_table.sort_index()

sorted_table.to_csv("output.csv")
